using System.Reflection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using TrendPilot.Api.Endpoints.Campaign;
using TrendPilot.Api.Endpoints.Marketdata;
using TrendPilot.Api.Endpoints.User;
using TrendPilot.Core.Application.Clients;
using TrendPilot.Core.Application.Services;
using TrendPilot.Infra.Configuration;
using TrendPilot.Infra.Schwab;

var builder = WebApplication.CreateBuilder(args);

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.EnableAnnotations();

    var oAuthDomain = builder.Configuration["Auth0:Domain"];
    var oAuthAudience = builder.Configuration["Auth0:Audience"];
    options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
    {
        Type = SecuritySchemeType.OAuth2,
        BearerFormat = "JWT",
        Flows = new OpenApiOAuthFlows
        {
            AuthorizationCode = new OpenApiOAuthFlow
            {
                AuthorizationUrl = new Uri($"{oAuthDomain}/authorize?audience={oAuthAudience}"),
                TokenUrl = new Uri($"{oAuthDomain}/oauth/token"),
                Scopes = new Dictionary<string, string>
                {
                    { "openid", "OpenId" }
                }
            }
        }
    });

    options.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "oauth2" }
            },
            new[] { "openid" }
        }
    });

    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    options.IncludeXmlComments(xmlPath);
});

// Add Authentication services
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.Authority = builder.Configuration["Auth0:Domain"];
        options.Audience = builder.Configuration["Auth0:Audience"];
        options.TokenValidationParameters = new TokenValidationParameters
        {
            NameClaimType = "userId"
        };
    });

builder.Services.AddAuthorization();

// Add Api.Infra components
builder.Services.AddInfraComponents(options =>
{
    options.CosmosEndpoint = builder.Configuration["CosmosDb:CosmosEndpoint"]!;
    options.CosmosKey = builder.Configuration["CosmosDb:CosmosKey"]!;
});

// Add Api.Infra.SchwabApi components (requires CosmosClient from Api.Infra)
builder.Services.Configure<SchwabApiClientFactoryOptions>(builder.Configuration.GetSection("SchwabApi"));
builder.Services.AddKeyedScoped<IBrokerageClientFactory, SchwabApiClientFactory>("Schwab", (provider, _) =>
{
    var options = provider.GetRequiredService<IOptions<SchwabApiClientFactoryOptions>>();
    var httpClientFactory = provider.GetRequiredService<IHttpClientFactory>();
    var cosmosClient = provider.GetRequiredService<CosmosClient>();
    var authenticationCache = cosmosClient.GetContainer("TrendPilot", "AuthenticationCache");
    return new SchwabApiClientFactory(options, httpClientFactory, authenticationCache);
});

// Add Core.Application services
builder.Services.AddScoped<UserListService>();
builder.Services.AddScoped<CampaignService>();
builder.Services.AddScoped<RegisterUserService>();
builder.Services.AddScoped<LinkBrokerageAccountService>();
builder.Services.AddScoped<BrokerageTransactionListService>();
builder.Services.AddScoped<StartCampaignService>();
builder.Services.AddScoped<AddCampaignTransactionService>();
builder.Services.AddScoped<CampaignReportService>();
builder.Services.AddScoped<PriceHistoryService>();

builder.Services.AddScoped<BrokerageClientFactoryResolver>();

var app = builder.Build();

// Map user endpoints
app.MapUserListEndpoint();
app.MapRegisterUserEndpoint();
app.MapUserEndpoint();
app.MapLinkBrokerageEndpoint();
app.MapBrokerageTransactionListEndpoint();

// Map campaign endpoints
app.MapCampaignListEndpoint();
app.MapStartCampaignEndpoint();
app.MapCampaignEndpoint();
app.MapCampaignReportEndpoint();
app.MapAddCampaignTransactionEndpoint();

// Map marketdata endpoints
app.MapPriceHistoryEndpoint();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI(settings =>
{
    settings.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
    settings.OAuthClientId(builder.Configuration["Auth0:ClientId"]);
    settings.OAuthClientSecret(builder.Configuration["Auth0:ClientSecret"]);
    settings.OAuthUsePkce();
});

// Disabling this temporarily since I think the docker container might be
// running behind a reverse proxy
// app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.Run();