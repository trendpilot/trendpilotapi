﻿using System.ComponentModel.DataAnnotations;

namespace TrendPilot.Api.Endpoints.Campaign;

public record StartCampaignRequest(string Symbol, [property: Required] IEnumerable<string> BrokerageTransactionIds)
{
    /// <summary>Symbol of the target stock/ETF</summary>
    /// <remarks>Don't use an options symbol. Use the symbol for the underlying asset.</remarks>
    [Required]
    public string Symbol { get; init; } = Symbol;
}