﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Models;
using TrendPilot.Core.Application.Services;

namespace TrendPilot.Api.Endpoints.Campaign;

public static class CampaignEndpoint
{
    public static void MapCampaignEndpoint(this IEndpointRouteBuilder app)
    {
        app.MapGet("/campaigns/{campaignId:guid}", Handler)
            .WithTags("Campaign Endpoints")
            .Produces<CampaignDto>()
            .Produces(StatusCodes.Status404NotFound);
    }

    /// <summary>
    ///     Get a campaign by identifier
    /// </summary>
    [Authorize]
    private static async Task<IResult> Handler(
        [FromServices] CampaignService campaignService,
        [FromRoute] Guid campaignId)
    {
        var result = await campaignService.Execute(campaignId);
        if (result.ErrorCode == ErrorCode.TargetNotFound)
            return Results.NotFound(result.ErrorMessage);

        return Results.Ok(result.Value);
    }
}