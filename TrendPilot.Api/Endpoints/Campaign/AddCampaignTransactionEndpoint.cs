﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TrendPilot.Core.Application.Services;

namespace TrendPilot.Api.Endpoints.Campaign;

public static class AddCampaignTransactionEndpoint
{
    public static void MapAddCampaignTransactionEndpoint(this IEndpointRouteBuilder app)
    {
        app.MapPost("/campaigns/{campaignId:guid}/transactions/add", Handler)
            .WithTags("Campaign Endpoints")
            .Produces(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest);
    }

    /// <summary>
    ///     Add transactions to the target campaign
    /// </summary>
    [Authorize]
    private static async Task<IResult> Handler(
        [FromServices] AddCampaignTransactionService addCampaignTransactionService,
        [FromRoute] Guid campaignId,
        [FromBody] IEnumerable<string> brokerageTransactionIds)
    {
        var result = await addCampaignTransactionService.Execute(campaignId, brokerageTransactionIds);
        if (result.ErrorMessage != null)
            return Results.BadRequest(result.ErrorMessage);

        return Results.NoContent();
    }
}