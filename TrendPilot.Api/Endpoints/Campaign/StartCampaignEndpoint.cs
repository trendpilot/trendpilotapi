﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Models;
using TrendPilot.Core.Application.Services;

namespace TrendPilot.Api.Endpoints.Campaign;

public static class StartCampaignEndpoint
{
    public static void MapStartCampaignEndpoint(this IEndpointRouteBuilder app)
    {
        app.MapPost("/campaigns/start", Handler)
            .WithTags("Campaign Endpoints")
            .Produces<CampaignDto>(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest);
    }

    /// <summary>
    ///     Start a new Campaign
    /// </summary>
    [Authorize]
    private static async Task<IResult> Handler(
        HttpContext context,
        [FromServices] StartCampaignService startCampaignService,
        [FromBody] StartCampaignRequest request)
    {
        var (symbol, brokerageTransactionIds) = request;

        var claimName = context.User.Identity?.Name;
        var userId = new Guid(claimName!);

        var result = await startCampaignService.Execute(userId, symbol, brokerageTransactionIds);
        if (result.ErrorCode == ErrorCode.InvalidRequest)
            return Results.BadRequest(result.ErrorMessage);

        return Results.Created($"campaigns/{result.Value?.Id}", result.Value);
    }
}