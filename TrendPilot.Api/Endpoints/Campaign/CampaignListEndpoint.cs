﻿using Microsoft.AspNetCore.Authorization;
using TrendPilot.Core.Application.Dtos;

namespace TrendPilot.Api.Endpoints.Campaign;

public static class CampaignListEndpoint
{
    public static void MapCampaignListEndpoint(this IEndpointRouteBuilder app)
    {
        app.MapGet("/campaigns", Handler)
            .WithTags("Campaign Endpoints")
            .Produces<IEnumerable<CampaignDto>>()
            .Produces(StatusCodes.Status400BadRequest);
    }

    /// <summary>
    ///     Get a list of campaigns
    /// </summary>
    [Authorize]
    private static async Task<IResult> Handler()
    {
        throw new NotImplementedException();
    }
}