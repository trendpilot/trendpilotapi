﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TrendPilot.Core.Application.Models;
using TrendPilot.Core.Application.Services;

namespace TrendPilot.Api.Endpoints.Campaign;

public static class CampaignReportEndpoint
{
    public static void MapCampaignReportEndpoint(this IEndpointRouteBuilder app)
    {
        app.MapGet("/campaigns/{campaignId:guid}/report", Handler)
            .WithTags("Campaign Endpoints")
            .Produces<CampaignReport>()
            .Produces(StatusCodes.Status400BadRequest);
    }

    /// <summary>
    ///     Get a campaign performance report
    /// </summary>
    [Authorize]
    private static async Task<IResult> Handler(
        [FromServices] CampaignReportService campaignReportService,
        [FromRoute] Guid campaignId)
    {
        var result = await campaignReportService.Execute(campaignId, "Schwab");
        if (result.ErrorMessage != null)
            return Results.NotFound(result.ErrorMessage);

        return Results.Ok(result.Value);
    }
}