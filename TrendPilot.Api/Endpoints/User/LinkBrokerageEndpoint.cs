﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Models;
using TrendPilot.Core.Application.Services;

namespace TrendPilot.Api.Endpoints.User;

public static class LinkBrokerageEndpoint
{
    public static void MapLinkBrokerageEndpoint(this IEndpointRouteBuilder app)
    {
        app.MapPost("/users/brokerage/link", Handler)
            .WithTags("User Endpoints")
            .Produces<UserDto>(StatusCodes.Status204NoContent)
            .Produces(StatusCodes.Status400BadRequest);
    }

    /// <summary>
    ///     Link an external brokerage account
    /// </summary>
    [Authorize]
    private static async Task<IResult> Handler(
        HttpContext context,
        [FromServices] LinkBrokerageAccountService linkBrokerageAccountService,
        [FromBody] LinkBrokerageRequest request)
    {
        var (userId, code) = request;

        var result = await linkBrokerageAccountService.Execute(userId, code);
        if (result.ErrorCode == ErrorCode.TargetNotFound)
            return Results.NotFound();
        if (result.ErrorCode == ErrorCode.InvalidRequest)
            return Results.BadRequest(result.ErrorMessage);

        return Results.Ok(result.Value);
    }
}