﻿using TrendPilot.Core.Application.Dtos;

namespace TrendPilot.Api.Endpoints.User;

public static class UserEndpoint
{
    public static void MapUserEndpoint(this IEndpointRouteBuilder app)
    {
        app.MapGet("/users/{userId:guid}", Handler)
            .WithTags("User Endpoints")
            .Produces<UserDto>()
            .Produces(StatusCodes.Status400BadRequest);
    }

    /// <summary>
    ///     Get a user by identifier
    /// </summary>
    private static Task<IResult> Handler()
    {
        throw new NotImplementedException();
    }
}