﻿using System.ComponentModel.DataAnnotations;

namespace TrendPilot.Api.Endpoints.User;

public record RegisterUserRequest(
    string AuthenticationId,
    [property: Required] string FirstName,
    [property: Required] string LastName,
    string Email)
{
    /// <summary>Corresponding user identifier from the OAuth provider</summary>
    [Required]
    public string AuthenticationId { get; init; } = AuthenticationId;

    /// <summary>Users' email; Should match the email in the OAuth provider</summary>
    [Required]
    [EmailAddress]
    public string Email { get; init; } = Email;
}