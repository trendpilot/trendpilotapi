﻿using System.ComponentModel.DataAnnotations;

namespace TrendPilot.Api.Endpoints.User;

public record LinkBrokerageRequest([property: Required] Guid UserId, string Code)
{
    /// <summary>Authorization code provided during authentication with a supported brokerage service</summary>
    /// <remarks>The code will be used to complete authentication/integration with the brokerage service</remarks>
    [Required]
    public string Code { get; init; } = Code;
}