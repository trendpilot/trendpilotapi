﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Services;

namespace TrendPilot.Api.Endpoints.User;

public static class UserListEndpoint
{
    public static void MapUserListEndpoint(this IEndpointRouteBuilder app)
    {
        app.MapGet("/users", Handler)
            .WithTags("User Endpoints")
            .Produces<IEnumerable<UserDto>>()
            .Produces(StatusCodes.Status400BadRequest);
    }

    /// <summary>
    ///     Get a list of users
    /// </summary>
    [Authorize]
    private static async Task<IResult> Handler(
        [FromServices] UserListService userListService)
    {
        var result = await userListService.Execute();
        if (result.ErrorCode != null)
            throw new Exception("Unexpected error");

        return Results.Ok(result.Value);
    }
}