﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Models;
using TrendPilot.Core.Application.Services;

namespace TrendPilot.Api.Endpoints.User;

public static class BrokerageTransactionListEndpoint
{
    public static void MapBrokerageTransactionListEndpoint(this IEndpointRouteBuilder app)
    {
        app.MapGet("/users/brokerage/{brokerageAccountId}/transactions", Handler)
            .WithTags("User Endpoints")
            .Produces<IEnumerable<BrokerageTransactionDto>>()
            .Produces(StatusCodes.Status400BadRequest);
    }

    /// <summary>
    ///     Get brokerage transactions
    /// </summary>
    /// <returns>A list of transactions for the specified criteria.</returns>
    [Authorize]
    private static async Task<IResult> Handler(
        HttpContext context,
        [FromServices] BrokerageTransactionListService brokerageTransactionListService,
        [FromRoute] string brokerageAccountId,
        [AsParameters] BrokerageTransactionListRequest request)
    {
        var (startDate, endDate, symbol) = request;

        var claimName = context.User.Identity?.Name;
        var userId = new Guid(claimName!);

        var result =
            await brokerageTransactionListService.Execute(userId, brokerageAccountId, startDate, endDate, symbol);
        if (result.ErrorCode == ErrorCode.InvalidRequest)
            return Results.BadRequest(result.ErrorMessage);
        if (result.ErrorCode == ErrorCode.Forbidden)
            return Results.Forbid();

        return Results.Ok(result.Value);
    }
}