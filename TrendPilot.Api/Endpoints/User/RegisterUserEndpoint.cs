﻿using Microsoft.AspNetCore.Mvc;
using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Services;

namespace TrendPilot.Api.Endpoints.User;

public static class RegisterUserEndpoint
{
    public static void MapRegisterUserEndpoint(this IEndpointRouteBuilder app)
    {
        app.MapPost("/users/register", Handler)
            .WithTags("User Endpoints")
            .Produces<UserDto>(StatusCodes.Status201Created)
            .Produces(StatusCodes.Status400BadRequest);
    }

    /// <summary>
    ///     Register as a new user
    /// </summary>
    /// <remarks>A corresponding user must exist in the OAuth provider.</remarks>
    private static async Task<IResult> Handler(
        [FromServices] RegisterUserService registerUserService,
        [FromBody] RegisterUserRequest request)
    {
        var (authenticationId, email, firstName, lastName) = request;

        var result = await registerUserService.Execute(authenticationId, email, firstName, lastName);
        if (result.ErrorCode != null)
            throw new Exception("Unexpected error");

        return Results.Created($"users/{result.Value?.Id}", result.Value);
    }
}