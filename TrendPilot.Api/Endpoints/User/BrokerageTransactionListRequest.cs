﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;

namespace TrendPilot.Api.Endpoints.User;

public record BrokerageTransactionListRequest(
    DateTime StartDate,
    DateTime EndDate,
    string? Symbol = null)
{
    /// <summary>
    ///     The start date. Can be date-only (e.g. 2025-01-01) or include a time part (e.g. 2025-01-01T12:00:00)
    /// </summary>
    [FromQuery]
    [Required]
    public DateTime StartDate { get; init; } = StartDate;

    /// <summary>
    ///     The end date. Can be date-only (e.g. 2025-01-01) or include a time part (e.g. 2025-01-01T12:00:00)
    /// </summary>
    [FromQuery]
    [Required]
    public DateTime EndDate { get; init; } = EndDate;

    /// <summary>
    ///     The ticker symbol.  Derivatives (e.g. options) for the symbol will be included.
    /// </summary>
    [FromQuery]
    public string? Symbol { get; init; } = Symbol;
}