﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Models;
using TrendPilot.Core.Application.Services;

namespace TrendPilot.Api.Endpoints.Marketdata;

public static class PriceHistoryEndpoint
{
    public static void MapPriceHistoryEndpoint(this IEndpointRouteBuilder app)
    {
        app.MapGet("/marketdata/{symbol}/pricehistory", Handler)
            .WithTags("Marketdata Endpoints")
            .Produces<IEnumerable<CandleSpanDto>>(StatusCodes.Status200OK)
            .Produces(StatusCodes.Status400BadRequest);
    }

    /// <summary>
    ///     Get the asset price history
    /// </summary>
    /// <returns>The assets' price history</returns>
    [Authorize]
    private static async Task<IResult> Handler(
        [FromServices] PriceHistoryService priceHistoryService,
        [FromRoute] string symbol,
        [AsParameters] PriceHistoryRequest request)
    {
        var result = await priceHistoryService.Execute(symbol, request.AnalysisPeriodAndSpan);
        if (result.ErrorCode == ErrorCode.InvalidRequest)
            return Results.BadRequest(result.ErrorMessage);

        return Results.Ok(result.Value);
    }
}