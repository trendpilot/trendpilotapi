﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace TrendPilot.Api.Endpoints.Marketdata;

public record PriceHistoryRequest(string AnalysisPeriodAndSpan)
{
    /// <summary>
    ///     Code representing the frame and interval (one of: 6m1d, 1y1d, 1d2m)
    /// </summary>
    [FromQuery(Name = "analysisPeriodAndSpan")]
    [Required]
    [RegularExpression("^(6m1d|1y1d|1d2m)$",
        ErrorMessage = "analysisPeriodAndSpan must be one of the following values: 6m1d, 1y1d, 1d2m")]
    [SwaggerSchema(Description = "A code representing the period of the analysis and span of candle data.")]
    public string AnalysisPeriodAndSpan { get; init; } = AnalysisPeriodAndSpan;
}