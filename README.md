﻿# TrendPilot API

This is the main application API for the TrendPilot service.

## Next Steps

- [x] Refactor Application Services
    - [x] Change `Service` suffix to `Feature` (or `UseCase`?)
    - [x] Make `User` and `Campaign` Services; Separate from `Feature` classes above.
    - [x] Make rename main method `Execute`
    - [x] Handle error scenarios
- [x] Attempt to make API endpoints look RESTful?
- [ ] Add more account info to a users' linked account
- [ ] Evaluate/Test endpoint HTTP response codes
- [ ] Endpoint to generate Campaign Report
- [ ] Implement logging
- [ ] Implement Cancellation Tokens
- [ ] Implement authentication (JWT or M2M?)
- [ ] Evaluate HTTP headers (e.g. Cache-Control, etc.)

## Organization

The API is organized into 4 projects and attempts to follow Clean Architecture principles, like Dependency Inversion.
Dependency Inversion is achieved by allowing the `Core.Domain` and `Core.Application` projects to define interfaces
related to persistence and remote services. The `Infra` project provides the implementations for these interfaces.

### TrendPilot.Core.Domain

The `Domain` project contains *all* business logic and attempts to follow the Domain Driven Design methodology.

### TrendPilot.Core.Application

The `Application` projects contains services that interact with the domain model. All application operations, commands
and queries, are carried out by these services.

### TrendPilot.Api

The API project handles concerns related to handling HTTP requests. All controller actions should only interact with
services in the `Application.Services` or `Application.UserCases` namespace. Controller actions should not interact with
the domain model directly or utilize any of the repositories or providers.

### TrendPilot.Api.Infra

The Infra project provides implementations for interfaces Core or API projects. The implementations for infrastructure
components should not contain business logic.
