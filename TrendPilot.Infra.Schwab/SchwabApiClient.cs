﻿using System.Net.Http.Headers;
using System.Net.Http.Json;
using TrendPilot.Core.Application.Clients;
using TrendPilot.Core.Application.Models;
using TrendPilot.Infra.Schwab.ApiModels;
using TrendPilot.Infra.Schwab.OAuthClient;

namespace TrendPilot.Infra.Schwab;

internal class SchwabApiClient : IBrokerageClient
{
    private const string Host = "api.schwabapi.com";
    private readonly HttpClient _httpClient;
    private readonly ISchwabOAuthClient _schwabOAuthClient;
    private string? _targetAccountHash;

    internal SchwabApiClient(HttpClient httpClient, ISchwabOAuthClient schwabOAuthClient)
    {
        _httpClient = httpClient;
        _schwabOAuthClient = schwabOAuthClient;
    }

    public async Task FinishAuthentication(string authorizationCode)
    {
        await _schwabOAuthClient.FinishAuthentication(authorizationCode);
    }

    public async Task<IEnumerable<BrokerageAccount>> GetAccounts()
    {
        var path = "/trader/v1/accounts";
        var responseContent = await SendGetRequestAsync<ApiAccountList>(path);
        if (responseContent == null)
            throw new Exception("Unable to get brokerage accounts");

        return ApiAccountList.ToApplicationModel(responseContent);
    }

    // The transactions are filtered after fetching so that options can be included also.
    public async Task<IEnumerable<BrokerageTransaction>> GetTransactions(DateTime startDate, DateTime endDate,
        string? symbol = null)
    {
        var accountHash = await GetAccountHash();
        var path = $"/trader/v1/accounts/{accountHash}/transactions";

        var queryParams = new Dictionary<string, string>
        {
            { "startDate", startDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") },
            { "endDate", endDate.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") },
            { "types", "TRADE" }
        };
        // if (symbol != null)
        //     queryParams["symbol"] = symbol;

        var responseContent = await SendGetRequestAsync<ApiTransactionList>(path, queryParams);
        if (responseContent == null)
            throw new Exception("Unable to get brokerage transactions");

        var transactions = ApiTransactionList.ToApplicationModel(responseContent);

        return transactions.Where(t => t.Symbol == symbol || t.Underlying == symbol);
    }

    public async Task<IEnumerable<BrokerageTransaction>> GetTransactions(IEnumerable<string> brokerageTransactionIds)
    {
        var fetchTransactionTasks = brokerageTransactionIds.Select(GetTransaction);

        var brokerageTransactions = await Task.WhenAll(fetchTransactionTasks);

        return brokerageTransactions;
    }

    public async Task<IEnumerable<Quote>> GetQuotes(IEnumerable<string> symbols)
    {
        var path = "/marketdata/v1/quotes";
        var queryParams = new Dictionary<string, string>
        {
            { "symbols", string.Join(",", symbols) }
        };

        var responseContent = await SendGetRequestAsync<ApiQuoteDictionary>(path, queryParams);
        if (responseContent == null)
            throw new Exception("Unable to get brokerage quotes");

        return ApiQuoteDictionary.ToApplicationModel(responseContent);
    }

    public async Task<IEnumerable<CandleSpan>> GetPriceHistory(string symbol, PriceAnalysisSetting analysisSetting)
    {
        var path = "/marketdata/v1/pricehistory";

        string periodType, frequencyType;
        int period, frequency;
        if (analysisSetting == PriceAnalysisSetting.SixMonthOneDay)
            (periodType, period, frequencyType, frequency) = ("month", 6, "daily", 1);
        else if (analysisSetting == PriceAnalysisSetting.OneYearOneDay)
            (periodType, period, frequencyType, frequency) = ("year", 1, "daily", 1);
        else if (analysisSetting == PriceAnalysisSetting.OneDayTwoMinute)
            (periodType, period, frequencyType, frequency) = ("day", 1, "minute", 2);
        else
            throw new Exception("Unsupported price frame and interval");

        var queryParams = new Dictionary<string, string>
        {
            { "symbol", symbol },
            { "periodType", periodType },
            { "period", period.ToString() },
            { "frequencyType", frequencyType },
            { "frequency", frequency.ToString() }
            // { "startDate", string.Empty },
            // { "endDate", string.Empty }
        };

        var responseContent = await SendGetRequestAsync<ApiPriceHistoryData>(path, queryParams);
        if (responseContent == null)
            throw new Exception("Unable to get price history data.");

        return responseContent.Candles.Select(ApiCandle.ToApplicationModel);
    }

    private async Task<BrokerageTransaction> GetTransaction(string transactionId)
    {
        var accountHash = await GetAccountHash();
        var path = $"/trader/v1/accounts/{accountHash}/transactions/{transactionId}";
        var apiTransaction = await SendGetRequestAsync<ApiTransaction>(path);
        if (apiTransaction == null)
            throw new Exception("Unable to get brokerage transaction");

        return ApiTransaction.ToApplicationModel(apiTransaction);
    }

    private async Task<ApiAccountNumberHashList> GetAccountNumbers()
    {
        var path = "/trader/v1/accounts/accountNumbers";
        var responseContent = await SendGetRequestAsync<ApiAccountNumberHashList>(path);
        if (responseContent == null)
            throw new Exception("Unable to get brokerage account number hashes");

        return responseContent;
    }

    private async Task<T?> SendGetRequestAsync<T>(string path, IDictionary<string, string>? queryParams = null)
    {
        var accessToken = await _schwabOAuthClient.GetAccessToken();
        // TODO: what should happen if a valid access token isn't available?
        if (accessToken == null)
            throw new Exception("Unable to get access token for brokerage API");

        var builder = new UriBuilder
        {
            Scheme = "https",
            Host = Host,
            Path = path
        };

        if (queryParams != null)
            builder.Query = string.Join("&", queryParams.Select(kvp =>
                $"{Uri.EscapeDataString(kvp.Key)}={Uri.EscapeDataString(kvp.Value)}"));

        var requestMessage = new HttpRequestMessage(HttpMethod.Get, builder.Uri);
        requestMessage.Headers.Authorization =
            new AuthenticationHeaderValue("Bearer", accessToken.Token);

        var responseMessage = await _httpClient.SendAsync(requestMessage);
        responseMessage.EnsureSuccessStatusCode();

        return await responseMessage.Content.ReadFromJsonAsync<T>();
    }

    private async Task<string> GetAccountHash()
    {
        if (_targetAccountHash != null)
            return _targetAccountHash;

        var accountNumbers = await GetAccountNumbers();

        // WARNING: We're just taking the first hash of the first account. What if the user has more than one Schwab
        // account.
        var accountHash = accountNumbers.First().HashValue;
        _targetAccountHash = accountHash;

        return accountHash;
    }
}