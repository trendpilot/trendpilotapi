﻿using System.Text.Json.Serialization;
using TrendPilot.Core.Application.Clients;
using TrendPilot.Infra.Schwab.Utilities;

namespace TrendPilot.Infra.Schwab.ApiModels;

internal class ApiTransactionList : List<ApiTransaction>
{
    public static IEnumerable<BrokerageTransaction> ToApplicationModel(ApiTransactionList list)
    {
        return list.Select(ApiTransaction.ToApplicationModel);
    }
}

internal class ApiTransaction
{
    public long ActivityId { get; set; }

    [JsonConverter(typeof(SchwabDateTimeJsonConverter))]
    public DateTime TradeDate { get; set; }

    public IEnumerable<TransferItem> TransferItems { get; set; } = new List<TransferItem>();

    public static BrokerageTransaction ToApplicationModel(ApiTransaction response)
    {
        // The 'TransferItems' contains 3 - 4 records representing commission and fees. Filtering them out here.
        var item = response.TransferItems.FirstOrDefault(i => i.Instrument.AssetType is "EQUITY" or "OPTION");

        return new BrokerageTransaction
        {
            BrokerageId = response.ActivityId.ToString(),
            Date = response.TradeDate,
            Symbol = item?.Instrument.Symbol ?? string.Empty,
            AssetType = item?.Instrument.AssetType ?? string.Empty,
            Underlying = item?.Instrument.AssetType is "OPTION"
                ? item.Instrument.Symbol.Split(' ')[0]
                : null,
            Quantity = item?.Amount ?? 0,
            Price = item?.Price ?? 0
        };
    }

    internal class TransferItem
    {
        public InstrumentData Instrument { get; set; } = new();
        public decimal Amount { get; set; }
        public decimal Cost { get; set; }
        public decimal Price { get; set; }

        internal class InstrumentData
        {
            public string AssetType { get; set; } = string.Empty;
            public long InstrumentId { get; set; }
            public string Symbol { get; set; } = string.Empty;
        }
    }
}