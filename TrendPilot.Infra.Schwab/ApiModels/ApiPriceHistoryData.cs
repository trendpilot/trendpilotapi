﻿using TrendPilot.Core.Application.Models;

namespace TrendPilot.Infra.Schwab.ApiModels;

internal class ApiPriceHistoryData
{
    // TODO: Probably shouldn't use the PriceInterval application model here. Define a specific one.
    public IEnumerable<ApiCandle> Candles { get; set; } = [];
}

internal class ApiCandle
{
    public decimal Open { get; set; }
    public decimal Close { get; set; }
    public decimal High { get; set; }
    public decimal Low { get; set; }
    public int Volume { get; set; }
    public int Timestamp { get; set; }

    public static CandleSpan ToApplicationModel(ApiCandle apiCandle)
    {
        var dateTimeOffset = DateTimeOffset.FromUnixTimeMilliseconds(apiCandle.Timestamp);
        var date = dateTimeOffset.DateTime;

        return new CandleSpan
        {
            Open = apiCandle.Open,
            Close = apiCandle.Close,
            High = apiCandle.High,
            Low = apiCandle.Low,
            Volume = apiCandle.Volume,
            Date = date
        };
    }
}