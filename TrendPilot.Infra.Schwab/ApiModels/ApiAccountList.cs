﻿using TrendPilot.Core.Application.Clients;

namespace TrendPilot.Infra.Schwab.ApiModels;

internal class ApiAccountList : List<Account>
{
    public static IEnumerable<BrokerageAccount> ToApplicationModel(ApiAccountList list)
    {
        return list.Select(Account.ToApplicationModel);
    }
}

internal class Account
{
    public SecuritiesAccount SecuritiesAccount { get; set; } = new();

    public static BrokerageAccount ToApplicationModel(Account account)
    {
        return SecuritiesAccount.ToApplicationModel(account.SecuritiesAccount);
    }
}

internal class SecuritiesAccount
{
    public string AccountNumber { get; set; } = string.Empty;

    public static BrokerageAccount ToApplicationModel(SecuritiesAccount securitiesAccount)
    {
        return new BrokerageAccount
        {
            AccountNumber = securitiesAccount.AccountNumber
        };
    }
}