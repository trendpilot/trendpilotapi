﻿namespace TrendPilot.Infra.Schwab.ApiModels;

internal class ApiAccountNumberHashList : List<AccountNumberHash>
{
}

internal class AccountNumberHash
{
    public string AccountNumber { get; set; } = string.Empty;
    public string HashValue { get; set; } = string.Empty;
}