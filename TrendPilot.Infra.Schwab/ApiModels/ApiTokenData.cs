using System.Text.Json.Serialization;

namespace TrendPilot.Infra.Schwab.ApiModels;

internal class ApiTokenData
{
    // From API Documentation:
    // {
    //     "expires_in": 1800, //Number of seconds access_token is valid for 
    //     "token_type": "Bearer",
    //     "scope": "api",
    //     "refresh_token": "{REFRESH_TOKEN_HERE}", //Valid for 7 days 
    //     "access_token": "{ACCESS_TOKEN_HERE}", //Valid for 30 minutes 
    //     "id_token": "{JWT_HERE}"
    // }

    [JsonPropertyName("expires_in")] public int ExpiresIn { get; set; }
    [JsonPropertyName("token_type")] public string TokenType { get; set; } = string.Empty;
    [JsonPropertyName("scope")] public string Scope { get; set; } = string.Empty;
    [JsonPropertyName("refresh_token")] public string RefreshToken { get; set; } = string.Empty;
    [JsonPropertyName("access_token")] public string AccessToken { get; set; } = string.Empty;
    [JsonPropertyName("id_token")] public string IdToken { get; set; } = string.Empty;
}