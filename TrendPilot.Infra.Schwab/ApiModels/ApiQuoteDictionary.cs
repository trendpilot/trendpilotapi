﻿using System.Text.Json.Serialization;
using TrendPilot.Core.Application.Models;

namespace TrendPilot.Infra.Schwab.ApiModels;

internal class ApiQuoteDictionary : Dictionary<string, ApiQuote>
{
    public static IEnumerable<Quote> ToApplicationModel(ApiQuoteDictionary dictionary)
    {
        return dictionary.Values.Select(ApiQuote.ToApplicationModel);
    }
}

internal class ApiQuote
{
    public string AssetMainType { get; set; } = string.Empty;
    public int Ssid { get; set; }
    public string Symbol { get; set; } = string.Empty;
    public bool RealTime { get; set; }
    public string QuoteType { get; set; } = string.Empty;
    public ReferenceData? Reference { get; set; }
    public FundamentalData? Fundamental { get; set; }
    public QuoteData? Quote { get; set; }
    public RegularData? Regular { get; set; }
    public ExtendedData? Extended { get; set; }

    public static Quote ToApplicationModel(ApiQuote apiQuoteResponse)
    {
        return new Quote
        {
            Symbol = apiQuoteResponse.Symbol,
            Price = apiQuoteResponse.Quote?.Mark ?? 0
        };
    }

    internal class ReferenceData
    {
        public string Cusip { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
        public string Exchange { get; set; } = string.Empty;
        public string ExchangeName { get; set; } = string.Empty;
    }

    internal class FundamentalData
    {
        public decimal Avg10DaysVolume { get; set; }
        public decimal Avg1YearVolume { get; set; }
        public decimal DivAmount { get; set; }
        public int DivFreq { get; set; }
        public decimal DivPayAmount { get; set; }
        public decimal DivYield { get; set; }
        public decimal Eps { get; set; }
        public decimal FundLeverageFactor { get; set; }
        public decimal PeRatio { get; set; }
    }

    internal class QuoteData
    {
        [JsonPropertyName("52WeekHigh")] public decimal FiftyTwoWeekHigh { get; set; }
        [JsonPropertyName("52WeekLow")] public decimal FiftyTwoWeekLow { get; set; }
        public string AskMicId { get; set; } = string.Empty;
        public decimal AskPrice { get; set; }
        public int AskSize { get; set; }
        public long AskTime { get; set; }
        public string BidMicId { get; set; } = string.Empty;
        public decimal BidPrice { get; set; }
        public int BidSize { get; set; }
        public long BidTime { get; set; }
        public decimal ClosePrice { get; set; }
        public decimal HighPrice { get; set; }
        public string LastMicId { get; set; } = string.Empty;
        public decimal LastPrice { get; set; }
        public int LastSize { get; set; }
        public decimal LowPrice { get; set; }
        public decimal Mark { get; set; }
        public decimal MarkChange { get; set; }
        public decimal MarkPercentChange { get; set; }
        public decimal NetChange { get; set; }
        public decimal NetPercentChange { get; set; }
        public decimal OpenPrice { get; set; }
        public long QuoteTime { get; set; }
        public string SecurityStatus { get; set; } = string.Empty;
        public long TotalVolume { get; set; }
        public long TradeTime { get; set; }
        public decimal Volatility { get; set; }
    }

    internal class RegularData
    {
        public decimal RegularMarketLastPrice { get; set; }
        public int RegularMarketLastSize { get; set; }
        public decimal RegularMarketNetChange { get; set; }
        public decimal RegularMarketPercentChange { get; set; }
        public long RegularMarketTradeTime { get; set; }
    }

    internal class ExtendedData
    {
        public decimal AskPrice { get; set; }
        public int AskSize { get; set; }
        public decimal BidPrice { get; set; }
        public int BidSize { get; set; }
        public decimal LastPrice { get; set; }
        public int LastSize { get; set; }
        public decimal Mark { get; set; }
        public long QuoteTime { get; set; }
        public long TotalVolume { get; set; }
        public long TradeTime { get; set; }
    }
}