﻿namespace TrendPilot.Infra.Schwab;

public class SchwabApiClientFactoryOptions
{
    public string TokenUri { get; set; } = string.Empty;
    public string ClientId { get; set; } = string.Empty;
    public string ClientSecret { get; set; } = string.Empty;
    public string RedirectUri { get; set; } = string.Empty;
}