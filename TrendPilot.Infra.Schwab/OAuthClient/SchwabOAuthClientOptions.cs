﻿namespace TrendPilot.Infra.Schwab.OAuthClient;

internal class SchwabOAuthClientOptions
{
    public string TokenUri { get; set; } = string.Empty;
    public string ClientId { get; set; } = string.Empty;
    public string ClientSecret { get; set; } = string.Empty;
    public string RedirectUri { get; set; } = string.Empty;
    public string PersistenceKey { get; set; } = string.Empty;
}