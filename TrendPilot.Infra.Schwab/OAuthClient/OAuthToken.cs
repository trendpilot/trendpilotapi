﻿namespace TrendPilot.Infra.Schwab.OAuthClient;

/// <summary>
///     A JWT token obtained from a 3rd-party brokerage authentication service.
/// </summary>
/// <param name="Token"></param>
/// <param name="ExpiresOn"></param>
internal record OAuthToken(string Token, DateTime ExpiresOn)
{
    public string Token { get; } = Token;
    public DateTime ExpiresOn { get; } = ExpiresOn;
}