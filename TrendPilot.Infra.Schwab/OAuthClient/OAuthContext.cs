﻿namespace TrendPilot.Infra.Schwab.OAuthClient;

/// <summary>
///     Represents the authentication artifacts for a 3rd-party brokerage service.
///     This class currently assumes the target service utilizes OAuth authentication. This may need to change if
///     integration requires a different form of authentication/authorization.
/// </summary>
/// <param name="PersistenceKey">Key to retrieve authentication from persistence.</param>
/// <param name="Scope">The tokens' scope.</param>
/// <param name="AccessToken">The OAuth access token.</param>
/// <param name="RefreshToken">The OAuth refresh token.</param>
internal record OAuthContext(
    string PersistenceKey,
    string Scope,
    OAuthToken AccessToken,
    OAuthToken RefreshToken)
{
    // "id" is required by CosmosDB.
    // WARNING: This model now contains properties related to CosmosDB persistence.
    public string id { get; } = PersistenceKey;
    public string PersistenceKey { get; } = PersistenceKey;
    public string Scope { get; } = Scope;
    public OAuthToken AccessToken { get; } = AccessToken;
    public OAuthToken RefreshToken { get; } = RefreshToken;
    public DateTime Timestamp { get; } = DateTime.UtcNow;
}