﻿using System.Net;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using Microsoft.Azure.Cosmos;
using TrendPilot.Infra.Schwab.ApiModels;

namespace TrendPilot.Infra.Schwab.OAuthClient;

internal class SchwabOAuthClient : ISchwabOAuthClient
{
    private readonly Container _cache;
    private readonly HttpClient _httpClient;
    private readonly SchwabOAuthClientOptions _options;

    internal SchwabOAuthClient(SchwabOAuthClientOptions options, HttpClient httpClient, Container cache)
    {
        _options = options;
        _httpClient = httpClient;
        _cache = cache;
    }

    // TODO: consider adding a cancellation token.
    public async Task FinishAuthentication(string authorizationCode)
    {
        var content = new Dictionary<string, string>
        {
            ["grant_type"] = "authorization_code",
            ["code"] = authorizationCode,
            ["redirect_uri"] = _options.RedirectUri
        };

        await FetchAndStoreAuthenticationContext(content);
    }

    public async Task<OAuthToken?> GetAccessToken()
    {
        var oAuthContext = await GetCachedOAuthContext();
        if (oAuthContext == null)
            return null;

        if (oAuthContext.AccessToken.ExpiresOn < DateTime.UtcNow)
            return await RefreshAuthentication(oAuthContext);

        return oAuthContext.AccessToken;
    }

    private async Task<OAuthToken> RefreshAuthentication(OAuthContext authContext)
    {
        var content = new Dictionary<string, string>
        {
            ["grant_type"] = "refresh_token",
            ["refresh_token"] = authContext.RefreshToken.Token
        };

        var accessToken = await FetchAndStoreAuthenticationContext(content);

        return accessToken;
    }

    private async Task<OAuthToken> FetchAndStoreAuthenticationContext(IDictionary<string, string> content)
    {
        var apiTokenData = await SendTokenRequestAsync(content);

        // Maybe I shouldn't be using the Core.Application.OAuthContext model for persistence.
        var oAuthContext = new OAuthContext(
            _options.PersistenceKey,
            apiTokenData.Scope,
            new OAuthToken(apiTokenData.AccessToken, DateTime.UtcNow.AddMinutes(30)),
            new OAuthToken(apiTokenData.RefreshToken, DateTime.UtcNow.AddDays(7)));

        await CacheOAuthContext(oAuthContext);

        return oAuthContext.AccessToken;
    }

    private async Task<ApiTokenData> SendTokenRequestAsync(IDictionary<string, string> content)
    {
        var requestMessage = new HttpRequestMessage(HttpMethod.Post, _options.TokenUri)
        {
            Content = new FormUrlEncodedContent(content)
        };

        var authenticationParameter = ToBase64EncodedString($"{_options.ClientId}:{_options.ClientSecret}");
        requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Basic", authenticationParameter);

        var responseMessage = await _httpClient.SendAsync(requestMessage);
        responseMessage.EnsureSuccessStatusCode();

        var apiTokenData = await responseMessage.Content.ReadFromJsonAsync<ApiTokenData>();
        if (apiTokenData == null)
            throw new Exception("Unable to read APIs' token response.");
        return apiTokenData;
    }

    private async Task CacheOAuthContext(OAuthContext oAuthContext)
    {
        await _cache.UpsertItemAsync(oAuthContext, new PartitionKey(oAuthContext.PersistenceKey));
    }

    private async Task<OAuthContext?> GetCachedOAuthContext()
    {
        try
        {
            var result = await _cache.ReadItemAsync<OAuthContext>(
                _options.PersistenceKey,
                new PartitionKey(_options.PersistenceKey)
            );

            return result.Resource;
        }
        catch (CosmosException ex) when (ex.StatusCode == HttpStatusCode.NotFound)
        {
            // return null if no cached authentication context is found.
            return null;
        }
    }

    private static string ToBase64EncodedString(string plainText)
    {
        var plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        return Convert.ToBase64String(plainTextBytes);
    }
}