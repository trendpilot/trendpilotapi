﻿namespace TrendPilot.Infra.Schwab.OAuthClient;

/// <summary>
///     Client to manage 3rd-party service authentication concerns.
/// </summary>
internal interface ISchwabOAuthClient
{
    Task FinishAuthentication(string authorizationCode);
    Task<OAuthToken?> GetAccessToken();
}