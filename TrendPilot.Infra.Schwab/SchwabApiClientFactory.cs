﻿using Microsoft.Extensions.Options;
using TrendPilot.Core.Application.Clients;
using TrendPilot.Infra.Schwab.OAuthClient;
using Container = Microsoft.Azure.Cosmos.Container;

namespace TrendPilot.Infra.Schwab;

public class SchwabApiClientFactory : IBrokerageClientFactory
{
    private readonly Container _authenticationCache;
    private readonly IHttpClientFactory _httpClientFactory;
    private readonly SchwabApiClientFactoryOptions _options;

    public SchwabApiClientFactory(IOptions<SchwabApiClientFactoryOptions> options, IHttpClientFactory httpClientFactory,
        Container authenticationCache)
    {
        _options = options.Value;
        _httpClientFactory = httpClientFactory;
        _authenticationCache = authenticationCache;
    }

    public Task<IBrokerageClient> MakeClient(string persistenceKey)
    {
        var oAuthClientOptions = new SchwabOAuthClientOptions
        {
            TokenUri = _options.TokenUri,
            ClientId = _options.ClientId,
            ClientSecret = _options.ClientSecret,
            RedirectUri = _options.RedirectUri,
            PersistenceKey = persistenceKey
        };

        var oAuthHttpClient = _httpClientFactory.CreateClient(nameof(SchwabOAuthClient));
        var oAuthClient = new SchwabOAuthClient(oAuthClientOptions, oAuthHttpClient, _authenticationCache);

        var apiHttpClient = _httpClientFactory.CreateClient(nameof(SchwabApiClient));
        var apiClient = new SchwabApiClient(apiHttpClient, oAuthClient);

        return Task.FromResult<IBrokerageClient>(apiClient);
    }
}