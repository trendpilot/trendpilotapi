﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace TrendPilot.Infra.Schwab.Utilities;

internal class SchwabDateTimeJsonConverter : JsonConverter<DateTime>
{
    public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
    {
        var rawDate = reader.GetString()!;
        var correctedDate = rawDate.Insert(rawDate.Length - 2, ":");
        return DateTime.Parse(correctedDate);
    }

    public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
    {
        throw new NotImplementedException();
    }
}