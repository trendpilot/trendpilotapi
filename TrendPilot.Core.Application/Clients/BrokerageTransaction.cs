﻿namespace TrendPilot.Core.Application.Clients;

public class BrokerageTransaction
{
    public string BrokerageId { get; set; } = string.Empty;
    public DateTime Date { get; set; }
    public string Symbol { get; set; } = string.Empty;
    public string AssetType { get; set; } = string.Empty;
    public string? Underlying { get; set; }
    public decimal Quantity { get; set; }
    public decimal Price { get; set; }
}