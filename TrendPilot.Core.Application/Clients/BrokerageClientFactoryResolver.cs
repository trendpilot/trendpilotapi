﻿using Microsoft.Extensions.DependencyInjection;

namespace TrendPilot.Core.Application.Clients;

public class BrokerageClientFactoryResolver
{
    private readonly IServiceProvider _serviceProvider;

    public BrokerageClientFactoryResolver(IServiceProvider serviceProvider)
    {
        _serviceProvider = serviceProvider;
    }

    public IBrokerageClientFactory GetFactory(string brokerageName)
    {
        return brokerageName switch
        {
            "Schwab" => _serviceProvider.GetRequiredKeyedService<IBrokerageClientFactory>("Schwab"),
            // "BrokerageB" => _serviceProvider.GetRequiredService<BrokerageBClientFactory>(),
            _ => throw new ArgumentException("Invalid brokerage name", nameof(brokerageName))
        };
    }
}