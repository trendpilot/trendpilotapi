﻿using TrendPilot.Core.Application.Models;

namespace TrendPilot.Core.Application.Clients;

// TODO: Declare expected Exceptions; implement exceptions in client
public interface IBrokerageClient
{
    Task FinishAuthentication(string authorizationCode);
    Task<IEnumerable<BrokerageAccount>> GetAccounts();

    Task<IEnumerable<BrokerageTransaction>>
        GetTransactions(DateTime startDate, DateTime endDate, string? symbol = null);

    Task<IEnumerable<BrokerageTransaction>> GetTransactions(IEnumerable<string> transactionIds);
    Task<IEnumerable<Quote>> GetQuotes(IEnumerable<string> symbols);

    /// <summary>
    ///     The method signature only allows specific requests and can be expanded in the future.
    /// </summary>
    Task<IEnumerable<CandleSpan>> GetPriceHistory(string symbol, PriceAnalysisSetting analysisSetting);
}