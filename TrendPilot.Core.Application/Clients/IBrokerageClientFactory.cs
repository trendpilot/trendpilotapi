﻿namespace TrendPilot.Core.Application.Clients;

public interface IBrokerageClientFactory
{
    Task<IBrokerageClient> MakeClient(string persistenceKey);
}