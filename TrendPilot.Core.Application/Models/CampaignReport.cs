﻿using TrendPilot.Core.Domain.CampaignContext;

namespace TrendPilot.Core.Application.Models;

public class CampaignReport
{
    private readonly List<Position> _positions;

    public CampaignReport(Campaign campaign, IEnumerable<Quote> quotes)
    {
        UserId = campaign.UserId;
        Name = campaign.Name;
        CreatedOn = campaign.CreatedOn;

        _positions = campaign.Transactions
            .GroupBy(t => t.Symbol)
            .Select(Position.FromTransactions)
            .ToList();

        Quotes = quotes.ToList();
    }

    public Guid UserId { get; }
    public string Name { get; }
    public DateTime CreatedOn { get; }
    public IReadOnlyList<Position> Positions => _positions;
    public IReadOnlyList<Quote> Quotes { get; }
}