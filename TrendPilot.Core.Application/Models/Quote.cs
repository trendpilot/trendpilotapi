﻿namespace TrendPilot.Core.Application.Models;

public class Quote
{
    public string Symbol { get; set; } = string.Empty;
    public decimal Price { get; set; }
}