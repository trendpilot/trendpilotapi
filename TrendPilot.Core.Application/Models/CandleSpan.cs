﻿namespace TrendPilot.Core.Application.Models;

// REMOVE THIS:
// Attempting to converge on appropriate terms for the duration and sample size of the price analysis.
// I'm testing out "Analysis Period" and "Candle Span"
public class CandleSpan
{
    public decimal Open { get; set; }
    public decimal Close { get; set; }
    public decimal High { get; set; }
    public decimal Low { get; set; }
    public int Volume { get; set; }
    public DateTime Date { get; set; }
}