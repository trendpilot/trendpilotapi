﻿using System.Collections;

namespace TrendPilot.Core.Application.Models;

public class ServiceResult<T> : IEnumerable
{
    private ServiceResult(T? value, ErrorCode? errorCode = null, string? errorMessage = null)
    {
        Value = value;
        ErrorCode = errorCode;
        ErrorMessage = errorMessage;
    }

    public T? Value { get; }
    public ErrorCode? ErrorCode { get; }
    public string? ErrorMessage { get; }

    public IEnumerator GetEnumerator()
    {
        throw new NotImplementedException();
    }

    public static ServiceResult<T> Ok(T value)
    {
        return new ServiceResult<T>(value);
    }

    public static ServiceResult<T> Error(ErrorCode errorCode, string errorMessage)
    {
        return new ServiceResult<T>(default, errorCode, errorMessage);
    }
}

public enum ErrorCode
{
    InvalidRequest = 400,
    Forbidden = 403,
    TargetNotFound = 404
}