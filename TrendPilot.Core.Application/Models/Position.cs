﻿using TrendPilot.Core.Domain.CampaignContext;

namespace TrendPilot.Core.Application.Models;

public class Position
{
    public Position(string symbol, decimal quantity, decimal avgCost)
    {
        Symbol = symbol;
        Quantity = quantity;
        AvgCost = avgCost;
    }

    public string Symbol { get; }
    public decimal Quantity { get; private set; }
    public decimal AvgCost { get; private set; }
    public decimal TotalCost => AvgCost * Quantity;

    public void AddTransaction(CampaignTransaction transaction)
    {
        if (transaction.Symbol != Symbol)
            throw new InvalidOperationException("Transaction symbol must match position symbol.");

        var nextQuantity = Quantity + transaction.Quantity;
        var nextAverageCost = 0m;
        if (nextQuantity != 0)
            nextAverageCost = (AvgCost * Quantity + transaction.Price * transaction.Quantity) / nextQuantity;

        Quantity = nextQuantity;
        AvgCost = nextAverageCost;
    }

    public static Position FromTransactions(IEnumerable<CampaignTransaction> transactions)
    {
        var transactionList = transactions.ToList();

        // Assert all transactions have the same symbol
        if (transactionList.Select(t => t.Symbol).Distinct().Count() > 1)
            throw new InvalidOperationException("All transactions must have the same symbol.");

        var position = new Position(transactionList.First().Symbol, 0, 0);
        foreach (var transaction in transactionList)
            position.AddTransaction(transaction);

        return position;
    }
}