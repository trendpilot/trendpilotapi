﻿namespace TrendPilot.Core.Application.Models;

public enum PriceAnalysisSetting
{
    SixMonthOneDay = 1,
    OneYearOneDay = 2,
    OneDayTwoMinute = 3
}