﻿using TrendPilot.Core.Domain.UserContext;

namespace TrendPilot.Core.Application.Dtos;

public class UserDto
{
    // private constructor to force clients to use the static factory method
    private UserDto()
    {
    }

    public required Guid Id { get; init; }
    public required string AuthenticationId { get; init; }
    public required string Email { get; init; }
    public required string FirstName { get; init; }
    public required string LastName { get; init; }
    public required IReadOnlyList<LinkedAccount> LinkedAccounts { get; init; }

    public static UserDto From(User user)
    {
        return new UserDto
        {
            Id = user.Id,
            AuthenticationId = user.AuthenticationId,
            Email = user.Email,
            FirstName = user.FirstName,
            LastName = user.LastName,
            LinkedAccounts = user.LinkedAccounts.ToList()
        };
    }
}