﻿using TrendPilot.Core.Application.Models;

namespace TrendPilot.Core.Application.Dtos;

public class CandleSpanDto
{
    public decimal Open { get; set; }
    public decimal Close { get; set; }
    public decimal High { get; set; }
    public decimal Low { get; set; }
    public decimal Volume { get; set; }
    public DateTime Date { get; set; }

    public static CandleSpanDto From(CandleSpan candleSpan)
    {
        return new CandleSpanDto
        {
            Open = candleSpan.Open,
            Close = candleSpan.Close,
            High = candleSpan.High,
            Low = candleSpan.Low,
            Volume = candleSpan.Volume,
            Date = candleSpan.Date
        };
    }
}