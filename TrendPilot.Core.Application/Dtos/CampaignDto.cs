﻿using TrendPilot.Core.Domain.CampaignContext;

namespace TrendPilot.Core.Application.Dtos;

public class CampaignDto
{
    // private constructor to force clients to use the static factory method
    private CampaignDto()
    {
    }

    public required Guid Id { get; init; }
    public required Guid UserId { get; init; }
    public required string Name { get; init; }
    public required DateTime CreatedOn { get; init; }
    public required IReadOnlyList<CampaignTransaction> Transactions { get; init; }

    public static CampaignDto From(Campaign campaign)
    {
        return new CampaignDto
        {
            Id = campaign.Id,
            UserId = campaign.UserId,
            Name = campaign.Name,
            CreatedOn = campaign.CreatedOn,
            Transactions = campaign.Transactions.ToList()
        };
    }
}