﻿using TrendPilot.Core.Application.Clients;

namespace TrendPilot.Core.Application.Dtos;

public class BrokerageTransactionDto
{
    // private constructor to force clients to use the static factory method
    private BrokerageTransactionDto()
    {
    }

    public required string BrokerageId { get; init; }
    public required DateTime Date { get; init; }
    public required string Symbol { get; init; }
    public required decimal Quantity { get; init; }
    public required decimal Price { get; init; }

    public static BrokerageTransactionDto From(BrokerageTransaction transaction)
    {
        return new BrokerageTransactionDto
        {
            BrokerageId = transaction.BrokerageId,
            Date = transaction.Date,
            Symbol = transaction.Symbol,
            Quantity = transaction.Quantity,
            Price = transaction.Price
        };
    }
}