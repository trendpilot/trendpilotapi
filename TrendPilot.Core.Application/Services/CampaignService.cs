﻿using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Models;
using TrendPilot.Core.Domain.CampaignContext;

namespace TrendPilot.Core.Application.Services;

public class CampaignService(ICampaignRepository campaignRepository)
{
    public async Task<ServiceResult<CampaignDto>> Execute(Guid campaignId)
    {
        var campaign = await campaignRepository.Get(campaignId);
        if (campaign == null)
            return ServiceResult<CampaignDto>.Error(ErrorCode.TargetNotFound, "Campaign not found.");

        var dto = CampaignDto.From(campaign);

        return ServiceResult<CampaignDto>.Ok(dto);
    }
}