﻿using System.Net;
using TrendPilot.Core.Application.Clients;
using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Models;

namespace TrendPilot.Core.Application.Services;

public class BrokerageTransactionListService(BrokerageClientFactoryResolver brokerageClientFactoryResolver)
{
    public async Task<ServiceResult<IEnumerable<BrokerageTransactionDto>>> Execute(Guid userId,
        string brokerageAccountId, DateTime startDate, DateTime endDate, string? symbol = null)
    {
        var brokerageClientFactory = brokerageClientFactoryResolver.GetFactory("Schwab");
        var brokerageClient = await brokerageClientFactory.MakeClient(userId.ToString());

        IEnumerable<BrokerageTransaction> brokerageTransactions;
        try
        {
            brokerageTransactions = await brokerageClient.GetTransactions(startDate, endDate, symbol);
        }
        catch (HttpRequestException ex) when (ex.StatusCode == HttpStatusCode.BadRequest)
        {
            return ServiceResult<IEnumerable<BrokerageTransactionDto>>.Error(ErrorCode.InvalidRequest,
                "Invalid request parameters.");
        }

        var dtos = brokerageTransactions
            .OrderBy(t => t.Date)
            .Select(BrokerageTransactionDto.From);

        return ServiceResult<IEnumerable<BrokerageTransactionDto>>.Ok(dtos);
    }
}