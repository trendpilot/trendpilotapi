﻿using TrendPilot.Core.Application.Clients;
using TrendPilot.Core.Application.Models;
using TrendPilot.Core.Domain.CampaignContext;

namespace TrendPilot.Core.Application.Services;

public class CampaignReportService(
    ICampaignRepository campaignRepository,
    BrokerageClientFactoryResolver brokerageClientFactoryResolver)
{
    // TODO: Should the return model be a DTO?
    public async Task<ServiceResult<CampaignReport>> Execute(Guid campaignId, string brokerageName)
    {
        var campaign = await campaignRepository.Get(campaignId);
        if (campaign == null)
            return ServiceResult<CampaignReport>.Error(ErrorCode.TargetNotFound, "Campaign not found.");

        var factory = brokerageClientFactoryResolver.GetFactory(brokerageName);
        var brokerageClient = await factory.MakeClient(campaign.UserId.ToString());

        var symbols = campaign.Transactions.Select(t => t.Symbol).Distinct();
        var quotes = await brokerageClient.GetQuotes(symbols);

        var report = new CampaignReport(campaign, quotes);
        return ServiceResult<CampaignReport>.Ok(report);
    }
}