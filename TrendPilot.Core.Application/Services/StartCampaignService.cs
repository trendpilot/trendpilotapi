﻿using System.Net;
using TrendPilot.Core.Application.Clients;
using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Models;
using TrendPilot.Core.Domain.CampaignContext;

namespace TrendPilot.Core.Application.Services;

public class StartCampaignService(
    BrokerageClientFactoryResolver brokerageClientFactoryResolver,
    ICampaignRepository campaignRepository)
{
    public async Task<ServiceResult<CampaignDto>> Execute(Guid userId, string symbol,
        IEnumerable<string> brokerageTransactionIds)
    {
        var campaign = new Campaign(userId, $"{symbol} Campaign");

        var brokerageClientFactory = brokerageClientFactoryResolver.GetFactory("Schwab");
        var brokerageClient = await brokerageClientFactory.MakeClient(userId.ToString());

        var transactionIds = brokerageTransactionIds.ToList();
        var brokerageTransactions = new List<BrokerageTransaction>();
        try
        {
            var transactions = await brokerageClient.GetTransactions(transactionIds);
            brokerageTransactions.AddRange(transactions);
        }
        // WARNING: This exception handling logic may not be valid for all Brokerage API scenarios.
        // WARNING: We're operating on the assumption that the IBrokerageClient is using the .NET HttpClient. This is 
        // probably a detail we shouldn't assume.
        catch (HttpRequestException ex) when (ex.StatusCode == HttpStatusCode.NotFound)
        {
            return ServiceResult<CampaignDto>.Error(ErrorCode.InvalidRequest,
                $"Invalid transaction ID in: {transactionIds}");
        }

        foreach (var transaction in brokerageTransactions)
            campaign.AddTransaction(transaction.BrokerageId, transaction.Date, transaction.Symbol, transaction.Quantity,
                transaction.Price);

        await campaignRepository.Persist(campaign);

        var dto = CampaignDto.From(campaign);

        return ServiceResult<CampaignDto>.Ok(dto);
    }
}