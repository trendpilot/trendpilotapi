using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Models;
using TrendPilot.Core.Domain.UserContext;

namespace TrendPilot.Core.Application.Services;

public class UserListService(IUserRepository userRepository)
{
    public async Task<ServiceResult<IEnumerable<UserDto>>> Execute()
    {
        var users = await userRepository.GetMany();

        var dtos = users.Select(UserDto.From);

        return ServiceResult<IEnumerable<UserDto>>.Ok(dtos);
    }
}