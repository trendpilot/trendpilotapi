﻿using System.Net;
using TrendPilot.Core.Application.Clients;
using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Models;

namespace TrendPilot.Core.Application.Services;

public class PriceHistoryService(BrokerageClientFactoryResolver brokerageClientFactoryResolver)
{
    public async Task<ServiceResult<IEnumerable<CandleSpanDto>>> Execute(string symbol, string analysisSetting)
    {
        var brokerageClientFactory = brokerageClientFactoryResolver.GetFactory("Schwab");
        var brokerageClient = await brokerageClientFactory.MakeClient(string.Empty);

        IEnumerable<CandleSpan> sessions;
        try
        {
            sessions = await brokerageClient.GetPriceHistory(symbol, PriceAnalysisSetting.OneYearOneDay);
        }
        catch (HttpRequestException ex) when (ex.StatusCode == HttpStatusCode.BadRequest)
        {
            return ServiceResult<IEnumerable<CandleSpanDto>>.Error(ErrorCode.InvalidRequest,
                "Invalid request parameters.");
        }

        var dtos = sessions.Select(CandleSpanDto.From);

        return ServiceResult<IEnumerable<CandleSpanDto>>.Ok(dtos);
    }
}