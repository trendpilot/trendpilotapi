﻿using System.Net;
using TrendPilot.Core.Application.Clients;
using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Models;
using TrendPilot.Core.Domain.UserContext;

namespace TrendPilot.Core.Application.Services;

/// <summary>
///     Allows a user to link an existing 3rd-party brokerage account.
/// </summary>
public class LinkBrokerageAccountService(
    BrokerageClientFactoryResolver brokerageClientFactoryResolver,
    IUserRepository userRepository)
{
    /// <summary>
    ///     Finishes the Authorization Code Flow (OAuth) for the 3rd-party service by exchanging an authorization code for
    ///     JWT tokens.
    /// </summary>
    /// <param name="userId">TrendPilots' user identifier.</param>
    /// <param name="authenticationCode">
    ///     The "code" parameter from the 3rd-party authentication service. It's available as a query param in the redirect
    ///     URL following a successful against the 3rd-party service.
    /// </param>
    /// <exception cref="Exception"></exception>
    public async Task<ServiceResult<UserDto>> Execute(Guid userId, string authenticationCode)
    {
        var user = await userRepository.Get(userId);
        if (user == null)
            return ServiceResult<UserDto>.Error(ErrorCode.TargetNotFound, "User not found.");

        var brokerageClientFactory = brokerageClientFactoryResolver.GetFactory("Schwab");
        var brokerageClient = await brokerageClientFactory.MakeClient(userId.ToString());

        try
        {
            await brokerageClient.FinishAuthentication(authenticationCode);
        }
        catch (HttpRequestException ex) when (ex.StatusCode == HttpStatusCode.BadRequest)
        {
            return ServiceResult<UserDto>.Error(ErrorCode.InvalidRequest, "Invalid authentication code.");
        }

        var accounts = await brokerageClient.GetAccounts();

        foreach (var account in accounts)
            user.AddLinkedAccount(BrokerageFirm.Schwab, account.AccountNumber);

        await userRepository.Persist(user);

        var dto = UserDto.From(user);

        return ServiceResult<UserDto>.Ok(dto);
    }
}