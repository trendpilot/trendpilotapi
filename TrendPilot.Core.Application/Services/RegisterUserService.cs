﻿using TrendPilot.Core.Application.Dtos;
using TrendPilot.Core.Application.Models;
using TrendPilot.Core.Domain.UserContext;

namespace TrendPilot.Core.Application.Services;

public class RegisterUserService(IUserRepository userRepository)
{
    public async Task<ServiceResult<UserDto>> Execute(string authenticationId, string email, string firstName,
        string lastName)
    {
        var user = new User(authenticationId, email, firstName, lastName);

        // WARNING: Errors caused by key constraints (e.g. email already in use) will error out without providing a
        // useful message to clients.
        await userRepository.Persist(user);

        var dto = UserDto.From(user);

        return ServiceResult<UserDto>.Ok(dto);
    }
}