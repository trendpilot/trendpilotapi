﻿using TrendPilot.Core.Domain.SharedKernel;

namespace TrendPilot.Core.Domain.CampaignContext;

public class Campaign : EntityBase<Guid>, IAggregateRoot
{
    private readonly List<CampaignTransaction> _transactions = [];

    public Campaign(Guid userId, string name) : base(Guid.NewGuid())
    {
        UserId = userId;
        Name = name;
        CreatedOn = DateTime.UtcNow;

        // TODO: Generate a Domain Event.
    }

    private Campaign(Guid id, Guid userId, string name, DateTime createdOn) : base(id)
    {
        UserId = userId;
        Name = name;
        CreatedOn = createdOn;
    }

    public Guid UserId { get; }
    public string Name { get; }
    public DateTime CreatedOn { get; }
    public IReadOnlyList<CampaignTransaction> Transactions => _transactions;

    public void AddTransaction(string brokerageId, DateTime date, string symbol, decimal quantity, decimal price)
    {
        // If the Campaign already includes the transaction, do nothing.
        if (_transactions.Any(t => t.BrokerageId == brokerageId))
            return;

        var transaction = new CampaignTransaction(brokerageId, Id, date, symbol, quantity, price);
        _transactions.Add(transaction);

        // Keep the list ordered by transaction date
        _transactions.Sort((t1, t2) => t1.Date.CompareTo(t2.Date));

        // TODO: Generate a Domain Event.
    }

    public static Campaign Hydrate(Guid campaignId, Guid userId, string name, DateTime createdOn,
        IEnumerable<CampaignTransaction> transactions)
    {
        var campaign = new Campaign(campaignId, userId, name, createdOn);
        campaign._transactions.AddRange(transactions);
        return campaign;
    }
}