﻿using TrendPilot.Core.Domain.SharedKernel;

namespace TrendPilot.Core.Domain.CampaignContext;

public class CampaignTransaction : EntityBase<Guid>
{
    public CampaignTransaction(string brokerageId, Guid campaignId, DateTime date, string symbol,
        decimal quantity, decimal price) : base(Guid.NewGuid())
    {
        BrokerageId = brokerageId;
        CampaignId = campaignId;
        Date = date;
        Symbol = symbol;
        Quantity = quantity;
        Price = price;
        CreatedOn = DateTime.UtcNow;
    }

    private CampaignTransaction(Guid id, string brokerageId, Guid campaignId, DateTime date, string symbol,
        decimal quantity, decimal price, DateTime createdOn) : base(id)
    {
        BrokerageId = brokerageId;
        CampaignId = campaignId;
        Date = date;
        Symbol = symbol;
        Quantity = quantity;
        Price = price;
        CreatedOn = createdOn;
    }

    public string BrokerageId { get; }
    public Guid CampaignId { get; }
    public DateTime Date { get; }
    public string Symbol { get; }
    public decimal Quantity { get; }
    public decimal Price { get; }
    public DateTime CreatedOn { get; }

    public static CampaignTransaction Hydrate(Guid id, string brokerageId, Guid campaignId, DateTime date,
        string symbol, decimal quantity, decimal price, DateTime createdOn)
    {
        return new CampaignTransaction(id, brokerageId, campaignId, date, symbol, quantity, price, createdOn);
    }
}