﻿namespace TrendPilot.Core.Domain.CampaignContext;

public interface ICampaignRepository
{
    public Task<Campaign?> Get(Guid campaignId);
    public Task Persist(Campaign campaign);
}