﻿using TrendPilot.Core.Domain.SharedKernel;

namespace TrendPilot.Core.Domain.UserContext;

/// <summary>
///     Represents an application user.
/// </summary>
public class User : EntityBase<Guid>, IAggregateRoot
{
    private readonly List<LinkedAccount> _linkedAccounts = [];

    public User(string authenticationId, string email, string firstName, string lastName) : base(Guid.NewGuid())
    {
        AuthenticationId = authenticationId;
        Email = email;
        FirstName = firstName;
        LastName = lastName;

        // TODO: Generate a Domain Event.
    }

    private User(Guid id, string authenticationId, string email, string firstName, string lastName) : base(id)
    {
        AuthenticationId = authenticationId;
        Email = email;
        FirstName = firstName;
        LastName = lastName;
    }

    public string AuthenticationId { get; }
    public string Email { get; }
    public string FirstName { get; }
    public string LastName { get; }
    public IReadOnlyList<LinkedAccount> LinkedAccounts => _linkedAccounts;

    public void AddLinkedAccount(BrokerageFirm brokerageFirm, string accountNumber)
    {
        // TODO: What if the linked account already exists?
        _linkedAccounts.Add(new LinkedAccount(brokerageFirm, accountNumber));

        // TODO: Generate a Domain Event?
    }

    public static User Hydrate(Guid userId, string authenticationId, string email, string firstName, string lastName,
        IEnumerable<LinkedAccount> linkedAccounts)
    {
        var user = new User(userId, authenticationId, email, firstName, lastName);
        user._linkedAccounts.AddRange(linkedAccounts);
        return user;
    }
}