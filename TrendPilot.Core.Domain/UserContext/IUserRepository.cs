﻿namespace TrendPilot.Core.Domain.UserContext;

/// <summary>
///     Contract for User model persistence.
/// </summary>
/// <remarks>
///     WARNING: Consider carefully before adding additional "read" methods. This interface is intended to be used in the
///     "write" code paths only. Reads can be implemented outside the Domain Model.
/// </remarks>
public interface IUserRepository
{
    public Task<IEnumerable<User>> GetMany(int limit = 100);
    public Task<User?> Get(Guid userId);
    public Task Persist(User user);
}