﻿namespace TrendPilot.Core.Domain.UserContext;

/// <summary>
///     Enum representing recognized 3rd-party brokerage services.
/// </summary>
public enum BrokerageFirm
{
    Schwab = 1
}