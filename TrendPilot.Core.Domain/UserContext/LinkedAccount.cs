﻿using TrendPilot.Core.Domain.SharedKernel;

namespace TrendPilot.Core.Domain.UserContext;

/// <summary>
///     A 3rd-party brokerage account that the user has chosen to link.
/// </summary>
public record LinkedAccount : IValueObject
{
    public LinkedAccount(BrokerageFirm brokerageFirm, string accountNumber)
    {
        BrokerageFirm = brokerageFirm;
        AccountNumber = accountNumber;
        DateLinked = DateTime.UtcNow;
    }

    public BrokerageFirm BrokerageFirm { get; }
    public string AccountNumber { get; }
    public DateTime DateLinked { get; }
}