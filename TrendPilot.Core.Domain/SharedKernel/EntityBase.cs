﻿namespace TrendPilot.Core.Domain.SharedKernel;

public class EntityBase<TId>
{
    public EntityBase(TId id)
    {
        Id = id;
    }

    public TId Id { get; }
}