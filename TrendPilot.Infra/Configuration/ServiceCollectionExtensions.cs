﻿using Microsoft.Azure.Cosmos;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using TrendPilot.Core.Domain.CampaignContext;
using TrendPilot.Core.Domain.UserContext;
using TrendPilot.Infra.CampaignContext;
using TrendPilot.Infra.UserContext;

namespace TrendPilot.Infra.Configuration;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddInfraComponents(this IServiceCollection services,
        Action<InfraOptions> configure)
    {
        services.Configure(configure); // adds IOptions<InfraOptions> to services

        // TODO: this may not be needed right now
        services.AddHttpClient(); // adds IHttpClientFactory to services

        // Configure CosmosClient
        // TODO: Check docs to see if CosmosClient should be added as a Singleton...
        services.AddSingleton<CosmosClient>(provider =>
        {
            var options = provider.GetRequiredService<IOptions<InfraOptions>>();
            return new CosmosClient(options.Value.CosmosEndpoint, options.Value.CosmosKey);
        });

        // Configure repositories
        services.AddScoped<IUserRepository>(provider =>
        {
            var cosmosClient = provider.GetRequiredService<CosmosClient>();
            var container = cosmosClient.GetContainer("TrendPilot", "Users");
            return new CosmosUserRepository(container);
        });

        services.AddScoped<ICampaignRepository>(provider =>
        {
            var cosmosClient = provider.GetRequiredService<CosmosClient>();
            var container = cosmosClient.GetContainer("TrendPilot", "Campaigns");
            return new CosmosCampaignRepository(container);
        });

        return services;
    }
}