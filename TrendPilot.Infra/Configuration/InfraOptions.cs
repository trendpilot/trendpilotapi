﻿namespace TrendPilot.Infra.Configuration;

public record InfraOptions
{
    public string CosmosEndpoint { get; set; } = string.Empty;
    public string CosmosKey { get; set; } = string.Empty;
}