﻿using System.Net;
using Microsoft.Azure.Cosmos;
using TrendPilot.Core.Domain.UserContext;
using User = TrendPilot.Core.Domain.UserContext.User;

namespace TrendPilot.Infra.UserContext;

internal class CosmosUserRepository : IUserRepository
{
    private readonly Container _userContainer;

    public CosmosUserRepository(Container userContainer)
    {
        _userContainer = userContainer;
    }

    public async Task<IEnumerable<User>> GetMany(int limit = 100)
    {
        var queryText = $"select * from documents d where d.UserId != null offset 0 limit {limit}";
        using var feed = _userContainer.GetItemQueryIterator<UserDocument>(queryText);

        List<User> users = [];
        while (feed.HasMoreResults)
        {
            var response = await feed.ReadNextAsync();
            users.AddRange(response.Select(UserDocument.ToUser));
        }

        return users;
    }

    public async Task<User?> Get(Guid userId)
    {
        try
        {
            var response = await _userContainer.ReadItemAsync<UserDocument>(
                userId.ToString(),
                new PartitionKey(userId.ToString())
            );

            return UserDocument.ToUser(response.Resource);
        }
        catch (CosmosException ex) when (ex.StatusCode == HttpStatusCode.NotFound)
        {
            return null;
        }
    }

    public async Task Persist(User user)
    {
        var document = UserDocument.FromUser(user);
        await _userContainer.UpsertItemAsync(document, new PartitionKey(user.Id.ToString()));
    }
}