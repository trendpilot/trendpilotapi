﻿using TrendPilot.Core.Domain.UserContext;

namespace TrendPilot.Infra.UserContext;

internal record UserDocument
{
    public string id { get; init; } = string.Empty;
    public string UserId { get; init; } = string.Empty;
    public string AuthenticationId { get; init; } = string.Empty;
    public string Email { get; init; } = string.Empty;
    public string FirstName { get; init; } = string.Empty;
    public string LastName { get; init; } = string.Empty;
    public IEnumerable<LinkedAccount> LinkedAccounts { get; init; } = new List<LinkedAccount>();

    public static UserDocument FromUser(User user)
    {
        return new UserDocument
        {
            id = user.Id.ToString(),
            UserId = user.Id.ToString(),
            AuthenticationId = user.AuthenticationId,
            Email = user.Email,
            FirstName = user.FirstName,
            LastName = user.LastName,
            LinkedAccounts = user.LinkedAccounts
        };
    }

    public static User ToUser(UserDocument document)
    {
        return User.Hydrate(new Guid(document.UserId), document.AuthenticationId, document.Email, document.FirstName,
            document.LastName, document.LinkedAccounts);
    }
}