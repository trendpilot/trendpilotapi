﻿using System.Net;
using Microsoft.Azure.Cosmos;
using TrendPilot.Core.Domain.CampaignContext;

namespace TrendPilot.Infra.CampaignContext;

internal class CosmosCampaignRepository : ICampaignRepository
{
    private readonly Container _campaignContainer;

    public CosmosCampaignRepository(Container campaignContainer)
    {
        _campaignContainer = campaignContainer;
    }

    public async Task<Campaign?> Get(Guid campaignId)
    {
        try
        {
            var response = await _campaignContainer.ReadItemAsync<CampaignDocument>(
                campaignId.ToString(),
                new PartitionKey(campaignId.ToString())
            );

            return CampaignDocument.ToCampaign(response.Resource);
        }
        catch (CosmosException ex) when (ex.StatusCode == HttpStatusCode.NotFound)
        {
            return null;
        }
    }

    public async Task Persist(Campaign campaign)
    {
        var document = CampaignDocument.FromCampaign(campaign);
        await _campaignContainer.UpsertItemAsync(document, new PartitionKey(campaign.Id.ToString()));
    }
}