﻿using TrendPilot.Core.Domain.CampaignContext;

namespace TrendPilot.Infra.CampaignContext;

public class CampaignTransactionDocument
{
    public string id { get; set; } = string.Empty;
    public string TransactionId { get; set; } = string.Empty;
    public string BrokerageId { get; set; } = string.Empty;
    public string CampaignId { get; set; } = string.Empty;
    public DateTime Date { get; set; }
    public string Symbol { get; set; } = string.Empty;
    public decimal Quantity { get; set; }
    public decimal Price { get; set; }
    public DateTime CreatedOn { get; set; }

    public static CampaignTransactionDocument FromCampaignTransaction(CampaignTransaction transaction)
    {
        return new CampaignTransactionDocument
        {
            id = transaction.Id.ToString(),
            TransactionId = transaction.Id.ToString(),
            BrokerageId = transaction.BrokerageId,
            CampaignId = transaction.CampaignId.ToString(),
            Date = transaction.Date,
            Symbol = transaction.Symbol,
            Quantity = transaction.Quantity,
            Price = transaction.Price,
            CreatedOn = transaction.CreatedOn
        };
    }

    public static CampaignTransaction ToCampaignTransaction(CampaignTransactionDocument document)
    {
        return CampaignTransaction.Hydrate(
            new Guid(document.id),
            document.BrokerageId,
            new Guid(document.CampaignId),
            document.Date,
            document.Symbol,
            document.Quantity,
            document.Price,
            document.CreatedOn);
    }
}