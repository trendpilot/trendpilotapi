﻿using TrendPilot.Core.Domain.CampaignContext;

namespace TrendPilot.Infra.CampaignContext;

public class CampaignDocument
{
    public string id { get; set; } = string.Empty;
    public string CampaignId { get; set; } = string.Empty;
    public string UserId { get; set; } = string.Empty;
    public string Name { get; set; } = string.Empty;
    public DateTime CreatedOn { get; set; }
    public List<CampaignTransactionDocument> Transactions { get; set; } = [];

    public static CampaignDocument FromCampaign(Campaign campaign)
    {
        return new CampaignDocument
        {
            id = campaign.Id.ToString(),
            UserId = campaign.UserId.ToString(),
            CampaignId = campaign.Id.ToString(),
            Name = campaign.Name,
            CreatedOn = campaign.CreatedOn,
            Transactions = campaign.Transactions.Select(CampaignTransactionDocument.FromCampaignTransaction).ToList()
        };
    }

    public static Campaign ToCampaign(CampaignDocument document)
    {
        var campaignTransactions = document.Transactions
            .Select(CampaignTransactionDocument.ToCampaignTransaction)
            .ToList();

        return Campaign.Hydrate(new Guid(document.CampaignId), new Guid(document.UserId), document.Name,
            document.CreatedOn, campaignTransactions);
    }
}